import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {User} from "../model/user.model";
import {Observable} from "rxjs/index";

@Injectable()
export class ApiService {

  constructor(private http: HttpClient) { }
  baseUrl: string = 'http://35.188.116.193:4201/api';

  login(loginPayload) : Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/authenticate`, loginPayload);
  }

  veirfyUser() : Observable<any> {
    let userId ='';
    if(window.localStorage.getItem('_id')) {
      userId = window.localStorage.getItem('_id');
      return this.http.get<any>(`${this.baseUrl}/verifyUser/${userId}`);
    }
  }

  getPolls() : Observable<any> {
      return this.http.get<any>(`${this.baseUrl}/poll`);
  }
  getPoll(pollId) : Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/poll/${pollId}`);
  }

  createPoll(pollPayload) : Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/poll`, pollPayload);
  }

  updatePoll(poll): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}/poll`,poll);
  }
}