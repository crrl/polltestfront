export interface User {
  userName: String
  password: String
  Role: Role
  polls: [Number]
}

enum Role {
  admin,
  normal
}