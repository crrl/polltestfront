import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ApiService} from "../../service/api.service";
import { element } from 'protractor';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-add-user',
  templateUrl: './poll-creation.component.html',
  styleUrls: ['./poll-creation.component.css']
})
export class AddPollComponent implements OnInit {
  pollName = '';
  pollItem;
  pollItems = [];
  constructor(private formBuilder: FormBuilder,private router: Router, private apiService: ApiService, private _snackBar: MatSnackBar) { }

  addForm: FormGroup;

  ngOnInit() {
    if(!window.localStorage.getItem('token')) {
      this.router.navigate(['login']);
      return;
    }
  }

  addItem() {
    let pollLength = this.pollItems.length;
    if (!this.pollItem) return;
    this.pollItems.push({
      name: this.pollItem
    });
    this.pollItem = "";
    console.log(this.pollItems);
  }

  removeItem(index) {
    this.pollItems.splice(index,1);
  }

  goToPollList() {
    this.router.navigate(['list-poll']);
  }
  
  saveNewPoll() {
    if (!this.pollName) {
      this._snackBar.open("Poll name field cannot be empty.", "OK", {
        duration: 3000,
      });
      return;
    } else if (this.pollItems.length < 2) {
      this._snackBar.open("Insert atleast two items for the poll creation.", "OK", {
        duration: 3000,
      });      return;
    }
    
    let poll = {
      pollName: this.pollName,
      pollItems: this.pollItems
    };

    this.apiService.createPoll(poll)
      .subscribe( data => {
        if(data.status === 200) {
          this.router.navigate(['list-poll']);
        }
      });
  }

}