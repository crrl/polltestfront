import { Component, OnInit , Inject} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";
import {User} from "../../model/user.model";
import {ApiService} from "../../service/api.service";

@Component({
  selector: 'app-edit-poll',
  templateUrl: './poll-edit.component.html',
  styleUrls: ['./poll-edit.component.css']
})
export class EditPollComponent implements OnInit {
  constructor(private formBuilder: FormBuilder,private router: Router, private apiService: ApiService, private route: ActivatedRoute) { }
  addForm: FormGroup;
  pollId = this.route.snapshot.paramMap.get('id');
  user = window.localStorage.getItem('_id');
  poll;
  options;
  selectedOption;
  selectedAnswer;
  pollName = '';
  ngOnInit() {
    if(!window.localStorage.getItem('token')) {
      this.router.navigate(['login']);
      return;
    }
    this.apiService.getPoll(this.pollId).subscribe(data => {
      if(data.status === 200) {
        this.poll = data.result;
        this.pollName = data.result.name;
        this.options = data.result.options;
        this.options.forEach((option, index)=> {
          option.progress = (option.userId.length / this.poll.usersVoted.length) * 100;
          option.userId.forEach((pollUser, userIndex) => {
            if (this.user === pollUser) {
              this.selectedAnswer = index;
            }
          });
        });        
      }
    },
    err => {
      window.localStorage.clear();
      this.router.navigate(['login']);
    });
  }

  goToPollList() {
    this.router.navigate(['list-poll']);
  }

  saveSelectedOption(selectedAnswer) {
    let newPollOptions = {
      pollId: this.pollId,
      selectedAnswer,
    }
    this.apiService.updatePoll(newPollOptions)
    .subscribe( data => {
      if(data.status === 200) {
        this.router.navigate(['list-poll']);
      }
    });
  }
  
}