import { Component, OnInit , Inject} from '@angular/core';
import {Router} from "@angular/router";
import {User} from "../../model/user.model";
import {ApiService} from "../../service/api.service";



@Component({
  selector: 'app-list-user',
  templateUrl: './poll-list.component.html',
  styleUrls: ['./poll-list.component.css']
})

export class ListPollComponent implements OnInit {
  displayedColumns: string[] = ['Answered', 'name', 'Answer'];
  users: User[];
  userRole = window.localStorage.getItem('role');
  polls;
  user = window.localStorage.getItem('_id');
  constructor(private router: Router, private apiService: ApiService) { }

  ngOnInit() {
    if(!window.localStorage.getItem('token')) {
      this.router.navigate(['login']);
      return;
    }

    this.apiService.getPolls().subscribe(data => {
      if(data.status === 200) {
        this.polls = data.result;
        
      }
    },
    err => {
      window.localStorage.clear();
      this.router.navigate(['login']);
    });
  }

  verifyUser() {
    this.apiService.veirfyUser().subscribe(data => {
      console.log(data);
      if(data.status === 200) {
        window.localStorage.removeItem('role');
        window.localStorage.setItem('role', data.result.role);
        if (data.result.role === 'admin') {
          this.router.navigate(['add-poll']);
        } else {
          window.localStorage.clear();
          this.router.navigate(['login']);
        }
      }
    },
    err => {
      window.localStorage.clear();
      this.router.navigate(['login']);
    });
  }

  getSelectedPoll(id) {
    this.router.navigate(['edit-poll', id]);
  }
}
