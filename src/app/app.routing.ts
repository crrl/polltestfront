import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import { ListPollComponent } from './poll/poll-list/poll-list.component';
import { AddPollComponent } from './poll/poll-creation/poll-creation.component';
import { EditPollComponent } from './poll/poll-edit/poll-edit.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'add-poll', component: AddPollComponent },
  { path: 'list-poll', component: ListPollComponent },
  { path: 'edit-poll/:id', component: EditPollComponent },
  {path : '', component : LoginComponent}
];

export const routing = RouterModule.forRoot(routes);