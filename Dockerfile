FROM node:12.10.0-alpine as build-deps
COPY . /usr/src/app
WORKDIR /usr/src/app

RUN yarn install
RUN yarn build


FROM nginx:1.12-alpine
COPY  --from=build-deps /usr/src/app/dist/out /usr/share/nginx/html
WORKDIR /usr/share/nginx/html
EXPOSE 80 443 4200
CMD ["nginx", "-g", "daemon off;"]
